package rafalmag.databaser.db;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.lessThan;
import static org.junit.Assert.assertThat;

import org.hibernate.classic.Session;
import org.junit.Test;

import rafalmag.databaser.db.entity.Issue;
import rafalmag.databaser.db.entity.Newspaper;
import rafalmag.databaser.db.entity.Word;
import rafalmag.databaser.db.entity.WordOccurrences;

//@Ignore
public class DbPurgerTest extends AbstractDbUnitTestDb {

	@Test
	public void should_purge() throws Exception {
		// given
		Session currentSession = sessionFactory.getCurrentSession();
		int init_size = currentSession.createCriteria(Word.class).list().size();

		// when
		// try {
		new DbPurger(currentSession).purge();
		// } catch (Exception e) {
		// // TODO: handle exception
		// e.printStackTrace();
		// }

		// then
		int after_size = currentSession.createCriteria(Word.class).list()
				.size();
		assertThat(after_size, lessThan(init_size));

		assertThat(currentSession.createCriteria(WordOccurrences.class).list()
				.size(), equalTo(0));
		assertThat(currentSession.createCriteria(Word.class).list().size(),
				equalTo(0));
		assertThat(currentSession.createCriteria(Issue.class).list().size(),
				equalTo(0));
		assertThat(
				currentSession.createCriteria(Newspaper.class).list().size(),
				equalTo(0));

	}
}
