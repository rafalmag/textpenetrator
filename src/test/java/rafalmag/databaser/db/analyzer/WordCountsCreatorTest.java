package rafalmag.databaser.db.analyzer;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;

import java.util.List;

import org.junit.Test;

import com.google.common.collect.ImmutableList;

public class WordCountsCreatorTest {

	@Test
	public void should_gather_words_from_multiple_newspapers() throws Exception {
		// given
		String word = "abc";
		String newspaper1 = "gw";
		Long occurancesInNewspaper1 = 2L;
		String newspaper2 = "nd";
		Long occurancesInNewspaper2 = 3L;

		// when
		WordCountsCreator words = new WordCountsCreator();
		words.putWord(word, occurancesInNewspaper1, newspaper1);
		words.putWord(word, occurancesInNewspaper2, newspaper2);

		List<String> header = words.getHeader();
		List<List<String>> wordCountsList = words.getWordCountsList();

		// then
		assertThat(header, hasSize(3));
		assertThat(header, hasItem("word"));
		assertThat(header, hasItem(newspaper1));
		assertThat(header, hasItem(newspaper2));

		List<String> headerExpected = ImmutableList.of("word", newspaper1,
				newspaper2);
		assertThat(header, equalTo(headerExpected));

		assertThat(wordCountsList, hasSize(1));
		List<String> wordCountListItemExpected = ImmutableList.of(word,
				occurancesInNewspaper1.toString(),
				occurancesInNewspaper2.toString());
		assertThat(wordCountsList, hasItem(wordCountListItemExpected));
	}

	@Test
	public void should_gather_words_from_multiple_newspapers2()
			throws Exception {
		// given
		String word1 = "abc";
		String word2 = "abcdef";
		String newspaper1 = "gw";
		Long occurancesInNewspaper1 = 2L;
		String newspaper2 = "nd";
		Long occurancesInNewspaper2 = 3L;

		// when
		WordCountsCreator words = new WordCountsCreator();
		words.putWord(word1, occurancesInNewspaper1, newspaper1);
		words.putWord(word2, occurancesInNewspaper2, newspaper2);

		List<String> header = words.getHeader();
		List<List<String>> wordCountsList = words.getWordCountsList();

		// then
		assertThat(header, hasSize(3));
		assertThat(header, hasItem("word"));
		assertThat(header, hasItem(newspaper1));
		assertThat(header, hasItem(newspaper2));

		List<String> headerExpected = ImmutableList.of("word", newspaper1,
				newspaper2);
		assertThat(header, equalTo(headerExpected));

		assertThat(wordCountsList, hasSize(2));
		List<String> wordCountListItemExpected1 = ImmutableList.of(word1,
				occurancesInNewspaper1.toString(), "0");
		assertThat(wordCountsList, hasItem(wordCountListItemExpected1));

		List<String> wordCountListItemExpected2 = ImmutableList.of(word2, "0",
				occurancesInNewspaper2.toString());
		assertThat(wordCountsList, hasItem(wordCountListItemExpected2));
	}

	@Test
	public void should_gather_words_from_nothing() throws Exception {
		// given
		WordCountsCreator words = new WordCountsCreator();

		List<String> header = words.getHeader();
		List<List<String>> wordCountsList = words.getWordCountsList();

		// then
		assertThat(header, hasSize(1));
		assertThat(header, hasItem("word"));
		assertThat(wordCountsList, hasSize(0));
	}

}
