package rafalmag.databaser.db.analyzer;

import rafalmag.databaser.morf.MorfUtils;
import rafalmag.databaser.morf.MorfeuszUtils;

public class Words2MorfeuszTest extends Words2Test {

	@Override
	protected MorfUtils getMorfUtils() {
		return new MorfeuszUtils();
	}

}
