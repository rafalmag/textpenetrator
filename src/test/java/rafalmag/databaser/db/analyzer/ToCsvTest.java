package rafalmag.databaser.db.analyzer;

import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import org.hibernate.classic.Session;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import rafalmag.databaser.db.AbstractDbUnitTestDb;

public class ToCsvTest extends AbstractDbUnitTestDb {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(ToCsvTest.class);

	@Test
	public void should_save_words_to_csv() throws Exception {
		// given
		Path tmp = Files.createTempFile("words", ".csv");

		try {
			Session session = sessionFactory.getCurrentSession();
			List<List<String>> rows = Words.getWords(session);
			// when
			ToCsv.save(tmp.toFile(), rows);

			// then
			List<String> strings = Files.readAllLines(tmp, ToCsv.CHARSET);
			assertThat(strings, not(hasSize(0)));
			assertThat(strings, hasSize(greaterThan(10)));
			assertThat(strings, hasSize(rows.size()));
			LOGGER.debug("{}", strings);
		} finally {
			Files.delete(tmp);
		}
	}

	@Test
	public void should_save_word_count_to_csv() throws Exception {
		// given
		Path tmp = Files.createTempFile("wordCount", ".csv");

		try {
			Session session = sessionFactory.getCurrentSession();
			List<List<String>> rows = Words.getWordCountsAsLists(session);
			// when
			ToCsv.save(tmp.toFile(), rows);

			// then
			List<String> strings = Files.readAllLines(tmp, ToCsv.CHARSET);
			assertThat(strings, not(hasSize(0)));
			assertThat(strings, hasSize(greaterThan(10)));
			assertThat(strings, hasSize(rows.size()));
			assertThat(strings,
					hasItem("\"word\";\"Gazeta Wyborcza\";\"Nasz Dziennik\""));
			LOGGER.trace("{}", strings);
		} finally {
			Files.delete(tmp);
		}
	}
}
