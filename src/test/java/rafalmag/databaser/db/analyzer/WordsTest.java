package rafalmag.databaser.db.analyzer;

import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;

import java.util.Date;
import java.util.List;

import org.hibernate.classic.Session;
import org.junit.Test;

import rafalmag.databaser.db.AbstractHibernateTestDb;
import rafalmag.databaser.db.DateUtil;
import rafalmag.databaser.db.entity.Issue;
import rafalmag.databaser.db.entity.Newspaper;
import rafalmag.databaser.db.entity.Word;

import com.google.common.collect.ImmutableList;

public class WordsTest extends AbstractHibernateTestDb {

	@Test
	public void should_show_all_words() throws Exception {
		// given
		Date date = new Date();
		String title = "title";
		Issue issue = new Issue(new Newspaper(title), date);
		final String word = "abc";
		Session session = sessionFactory.getCurrentSession();
		session.save(new Word(issue, word));
		session.save(new Word(issue, word + word));

		// when
		List<List<String>> result = Words.getWords(session);

		// then
		assertThat(
				result,
				hasItem((List<String>) ImmutableList.of(title,
						DateUtil.toString(date), word)));
		assertThat(
				result,
				hasItem((List<String>) ImmutableList.of(title,
						DateUtil.toString(date), word + word)));
		assertThat(result, hasSize(2));
	}

}
