package rafalmag.databaser.db.analyzer;

import rafalmag.databaser.morf.MorfUtils;
import rafalmag.databaser.morf.MorfologikUtils;

public class Words2MorfologikTest extends Words2Test {

	@Override
	protected MorfUtils getMorfUtils() {
		return new MorfologikUtils();
	}

}
