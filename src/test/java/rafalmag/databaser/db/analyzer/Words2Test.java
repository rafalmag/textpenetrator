package rafalmag.databaser.db.analyzer;

import static org.hamcrest.Matchers.hasItem;
import static org.junit.Assert.assertThat;

import java.util.Date;
import java.util.List;

import org.hibernate.classic.Session;
import org.junit.Test;

import rafalmag.databaser.db.AbstractHibernateTestDb;
import rafalmag.databaser.db.entity.Issue;
import rafalmag.databaser.db.entity.Newspaper;
import rafalmag.databaser.db.entity.Word;
import rafalmag.databaser.db.entity.WordOccurrences;
import rafalmag.databaser.morf.MorfUtils;

public abstract class Words2Test extends AbstractHibernateTestDb {

	@Test
	public void should_count_words_from_one_issue() throws Exception {
		// given
		String title = "title";
		Newspaper newspaper = new Newspaper(title);
		Issue issue = new Issue(newspaper, new Date());
		final String word = "abc";
		Session session = sessionFactory.getCurrentSession();
		session.save(new Word(issue, word));
		session.save(new Word(issue, word));

		// when
		Words.recountGetWordsCountsObjects(session, getMorfUtils());
		session.flush();
		@SuppressWarnings("unchecked")
		List<WordOccurrences> list = session.createCriteria(
				WordOccurrences.class).list();

		// then
		WordOccurrences wo = new WordOccurrences();
		wo.setWord(getMorfUtils().getLemma(word));
		wo.addOccurrences(newspaper, 2L);
		assertThat(list, hasItem(wo));
	}

	abstract protected MorfUtils getMorfUtils();

	@Test
	public void should_count_words_from_one_newspaper_two_issues()
			throws Exception {
		// given
		String title = "title";
		Newspaper newspaper = new Newspaper(title);
		Issue issue1 = new Issue(newspaper, new Date(1L));
		Issue issue2 = new Issue(newspaper, new Date(2L));
		final String word = "abc";
		Session session = sessionFactory.getCurrentSession();
		session.save(new Word(issue1, word));
		session.save(new Word(issue2, word));

		// when
		Words.recountGetWordsCountsObjects(session, getMorfUtils());
		session.flush();
		@SuppressWarnings("unchecked")
		List<WordOccurrences> list = session.createCriteria(
				WordOccurrences.class).list();

		// then
		WordOccurrences wo = new WordOccurrences();
		wo.setWord(getMorfUtils().getLemma(word));
		wo.addOccurrences(newspaper, 2L);
		assertThat(list, hasItem(wo));
	}

	@Test
	public void should_count_words_from_newspapers_same() throws Exception {
		// given
		String title1 = "title1";
		String title2 = "title2";
		Newspaper newspaper1 = new Newspaper(title1);
		Newspaper newspaper2 = new Newspaper(title2);
		Issue issue1 = new Issue(newspaper1, new Date());
		Issue issue2 = new Issue(newspaper2, new Date());
		final String word = "abc";
		Session session = sessionFactory.getCurrentSession();
		session.save(new Word(issue1, word));
		session.save(new Word(issue2, word));

		// when
		Words.recountGetWordsCountsObjects(session, getMorfUtils());
		session.flush();
		@SuppressWarnings("unchecked")
		List<WordOccurrences> list = session.createCriteria(
				WordOccurrences.class).list();

		// then
		WordOccurrences wo = new WordOccurrences();
		wo.setWord(getMorfUtils().getLemma(word));
		wo.addOccurrences(newspaper1, 1L);
		wo.addOccurrences(newspaper2, 1L);
		assertThat(list, hasItem(wo));
	}

	@Test
	public void should_count_words_from_newspapers_mixed_forms()
			throws Exception {
		// given
		String title1 = "title1";
		String title2 = "title2";
		Newspaper newspaper1 = new Newspaper(title1);
		Newspaper newspaper2 = new Newspaper(title2);
		Issue issue1 = new Issue(newspaper1, new Date());
		Issue issue2 = new Issue(newspaper2, new Date());
		Session session = sessionFactory.getCurrentSession();
		session.save(new Word(issue1, "Polska"));
		session.save(new Word(issue2, "Polsko"));

		// when
		Words.recountGetWordsCountsObjects(session, getMorfUtils());
		session.flush();
		@SuppressWarnings("unchecked")
		List<WordOccurrences> list = session.createCriteria(
				WordOccurrences.class).list();

		// then
		WordOccurrences wo = new WordOccurrences();
		wo.setWord("polska");
		wo.addOccurrences(newspaper1, 1L);
		wo.addOccurrences(newspaper2, 1L);
		assertThat(list, hasItem(wo));
	}

	@Test
	public void should_count_words_from_newspapers_mixed_cases()
			throws Exception {
		// given
		String title1 = "title1";
		String title2 = "title2";
		Newspaper newspaper1 = new Newspaper(title1);
		Newspaper newspaper2 = new Newspaper(title2);
		Issue issue1 = new Issue(newspaper1, new Date());
		Issue issue2 = new Issue(newspaper2, new Date());
		Session session = sessionFactory.getCurrentSession();
		session.save(new Word(issue1, "duży"));
		session.save(new Word(issue2, "Duży"));

		// when
		Words.recountGetWordsCountsObjects(session, getMorfUtils());
		session.flush();
		@SuppressWarnings("unchecked")
		List<WordOccurrences> list = session.createCriteria(
				WordOccurrences.class).list();

		// then
		WordOccurrences wo = new WordOccurrences();
		wo.setWord("duży");
		wo.addOccurrences(newspaper1, 1L);
		wo.addOccurrences(newspaper2, 1L);
		assertThat(list, hasItem(wo));
	}

}
