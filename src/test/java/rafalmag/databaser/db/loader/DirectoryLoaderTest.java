package rafalmag.databaser.db.loader;

import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

import org.dbunit.DatabaseUnitException;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.hibernate.classic.Session;
import org.hibernate.jdbc.Work;
import org.junit.Ignore;
import org.junit.Test;

import rafalmag.databaser.db.AbstractHibernateTestDb;
import rafalmag.databaser.db.analyzer.ToCsv;
import rafalmag.databaser.db.analyzer.Words;
import rafalmag.databaser.db.entity.Issue;
import rafalmag.databaser.db.entity.Newspaper;
import rafalmag.databaser.db.entity.Word;
import rafalmag.databaser.db.entity.WordOccurrences;
import rafalmag.databaser.morf.MorfologikUtils;

import com.google.common.base.Throwables;

public class DirectoryLoaderTest extends AbstractHibernateTestDb {

	@Test
	public void should_load_directory() throws Exception {
		FileLoader mockFileLoader = mock(FileLoader.class);
		loadDirectoryTest(mockFileLoader);

		verify(mockFileLoader, times(4)).load(any(File.class),
				any(Newspaper.class));
	}

	protected void loadDirectoryTest(FileLoader fileLoader)
			throws URISyntaxException, IOException, ParseException {
		// given
		File gwDir = new File(this.getClass().getResource("/Gazeta Wyborcza")
				.toURI());
		assertThat(gwDir.isDirectory(), is(true));
		File ndDir = new File(this.getClass().getResource("/Nasz Dziennik")
				.toURI());
		assertThat(ndDir.isDirectory(), is(true));

		// when
		DirectoryLoader directoryLoader = new DirectoryLoader(
				sessionFactory.getCurrentSession());
		directoryLoader.setFileLoader(fileLoader);
		directoryLoader.loadDirectory(gwDir);
		directoryLoader.loadDirectory(ndDir);

		// then
		@SuppressWarnings("unchecked")
		List<Newspaper> newspaper = sessionFactory.getCurrentSession()
				.createCriteria(Newspaper.class).list();
		assertThat(newspaper, hasItem(new Newspaper("Gazeta Wyborcza")));
		assertThat(newspaper, hasItem(new Newspaper("Nasz Dziennik")));
	}

	@Ignore
	// use only to generate gw0106.xml
	@Test
	public void createDbDumpAfter_should_load_directory() throws Exception {
		Session session = sessionFactory.getCurrentSession();

		loadDirectoryTest(new FileLoader(session));

		Words.recountGetWordsCountsObjects(session, new MorfologikUtils());
		session.flush();

		session.doWork(new Work() {

			@Override
			public void execute(Connection connection) throws SQLException {
				try (Writer writer = new OutputStreamWriter(
						new FileOutputStream("gw0106.xml"), ToCsv.CHARSET)) {
					IDatabaseConnection databaseConnection = new DatabaseConnection(
							connection);
					// full database export
					IDataSet fullDataSet = databaseConnection.createDataSet(new String[] {
							Newspaper.class.getSimpleName().toUpperCase(),
							Issue.class.getSimpleName().toUpperCase(),
							Word.class.getSimpleName().toUpperCase(),
							WordOccurrences.class.getSimpleName().toUpperCase(),
							(WordOccurrences.class.getSimpleName() + "_occurrences")
									.toUpperCase() });

					FlatXmlDataSet.write(fullDataSet, writer);
				} catch (IOException | DatabaseUnitException ex) {
					throw Throwables.propagate(ex);
				}
			}
		});
	}
}
