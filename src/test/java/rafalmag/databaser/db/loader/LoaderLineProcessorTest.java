package rafalmag.databaser.db.loader;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;

import java.util.Date;
import java.util.List;

import org.junit.Test;

import rafalmag.databaser.db.AbstractHibernateTestDb;
import rafalmag.databaser.db.entity.Issue;
import rafalmag.databaser.db.entity.Newspaper;
import rafalmag.databaser.db.entity.Word;

import com.google.common.base.Joiner;

public class LoaderLineProcessorTest extends AbstractHibernateTestDb {

	@Test
	public void should_get_words_from_line_pl_chars() {
		// given
		String line = "Zażółć gęślą jaźń";

		// when
		List<String> wordsList = LoaderLineProcessor.getWords(line);

		// then
		assertThat(wordsList, hasItems("Zażółć", "gęślą", "jaźń"));
		String words = Joiner.on(' ').join(wordsList);
		assertThat(words, equalTo(line));
	}

	@Test
	public void should_get_words_from_tricky_line() throws Exception {
		// given
		String input = "Ala ma kota. Ko\nt, 7 m\ru; ale(b8le[b9le.]10!?";

		// when
		List<String> list = LoaderLineProcessor.getWords(input);

		// then
		assertThat(list, hasItem("Ala"));
		assertThat(list, hasItem("ma"));
		assertThat(list, hasItem("kota"));
		assertThat(list, hasItem("Ko"));
		assertThat(list, hasItem("t"));
		assertThat(list, not(hasItem("7")));
		assertThat(list, hasItem("m"));
		assertThat(list, hasItem("u"));
		assertThat(list, hasItem("ale"));
		assertThat(list, hasItem("b8le"));
		assertThat(list, hasItem("b9le"));
		assertThat(list, not(hasItem("10")));
		assertThat(list, hasItem("!"));
		assertThat(list, hasItem("?"));
		assertThat(list, not(hasItem("")));
		assertThat(list, not(hasItem(" ")));
		assertThat(list, not(hasItem(".")));
		assertThat(list, not(hasItem(",")));
		assertThat(list, not(hasItem(";")));
		assertThat(list, not(hasItem("mu")));
		assertThat(list, not(hasItem("Kot")));

		assertThat(list.size(), is(12));
	}

	@Test
	public void should_load_line_to_db() throws Exception {
		// given
		String line = "Zażółć gęślą jaźń";
		Issue issue = new Issue(new Newspaper("gw"), new Date());
		sessionFactory.getCurrentSession().save(issue);

		// when
		LoaderLineProcessor loaderLineProcessor = new LoaderLineProcessor(
				sessionFactory.getCurrentSession(), issue);
		loaderLineProcessor.processLine(line);

		// then
		@SuppressWarnings("unchecked")
		List<Word> words = sessionFactory.getCurrentSession()
				.createCriteria(Word.class).list();
		String wordsFromDb = Joiner.on(' ').join(words);

		assertThat(wordsFromDb, equalTo(line));
	}
}
