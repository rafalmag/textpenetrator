package rafalmag.databaser.db.loader;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.io.File;
import java.net.URL;
import java.util.Calendar;
import java.util.List;

import org.junit.Test;

import rafalmag.databaser.db.AbstractHibernateTestDb;
import rafalmag.databaser.db.entity.Newspaper;
import rafalmag.databaser.db.entity.Word;

public class FileLoaderTest extends AbstractHibernateTestDb {

	@Test
	public void should_read_test_data() throws Exception {
		// given

		// when
		URL input = this.getClass()
				.getResource("/Gazeta Wyborcza/20100208.doc");

		// then
		assertThat(input, notNullValue());
	}

	@Test
	public void should_load_file_to_db() throws Exception {
		// given
		URL input = this.getClass()
				.getResource("/Gazeta Wyborcza/20100208.doc");
		File file = new File(input.toURI());
		Newspaper newspaper = new Newspaper("Gazeta Wyborcza");

		// when
		new FileLoader(sessionFactory.getCurrentSession())
				.load(file, newspaper);

		// then
		@SuppressWarnings("unchecked")
		List<Word> word = sessionFactory.getCurrentSession()
				.createCriteria(Word.class).list();

		assertThat(word.get(0).getIssue().getNewspaper().getTitle(),
				is("Gazeta Wyborcza"));

		Calendar cal = Calendar.getInstance();
		cal.clear();
		cal.set(Calendar.YEAR, 2010);
		cal.set(Calendar.MONTH, Calendar.AUGUST);
		cal.set(Calendar.DAY_OF_MONTH, 2);
		assertThat(word.get(0).getIssue().getDate(), equalTo(cal.getTime()));

		assertThat(word.get(0).getWord(), equalTo("Śledztwo"));

		printWords();

		printIssues();

		printNewspapers();
	}

}
