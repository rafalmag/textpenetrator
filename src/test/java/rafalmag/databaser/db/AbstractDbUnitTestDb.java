package rafalmag.databaser.db;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.hibernate.Session;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;

import rafalmag.databaser.db.analyzer.ToCsv;

public abstract class AbstractDbUnitTestDb extends AbstractHibernateTestDb {

	static private IDataSet dataSet;

	@BeforeClass
	public static void setUpDbUnitDataSet() throws Exception {
		File f = new File(AbstractDbUnitTestDb.class.getResource("/gw0106.xml")
				.toURI());
		try (Reader reader = new InputStreamReader(new FileInputStream(f),
				ToCsv.CHARSET)) {
			dataSet = new FlatXmlDataSetBuilder().build(reader);
		}
	}

	@Before
	public void setUpDbUnit() throws Exception {
		Session s = sessionFactory.getCurrentSession();
		@SuppressWarnings("deprecation")
		IDatabaseConnection conn = new DatabaseConnection(s.connection());
		DatabaseOperation.INSERT.execute(conn, dataSet);
	}

	@After
	public void cleanDbUnit() throws Exception {
		Session s = sessionFactory.getCurrentSession();
		@SuppressWarnings("deprecation")
		IDatabaseConnection conn = new DatabaseConnection(s.connection());
		DatabaseOperation.DELETE_ALL.execute(conn, dataSet);
	}

}
