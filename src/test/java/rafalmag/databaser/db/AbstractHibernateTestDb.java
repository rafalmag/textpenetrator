package rafalmag.databaser.db;

import java.net.URL;
import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import rafalmag.databaser.db.entity.Issue;
import rafalmag.databaser.db.entity.Newspaper;
import rafalmag.databaser.db.entity.Word;

public abstract class AbstractHibernateTestDb {
	static protected SessionFactory sessionFactory;

	private static final Logger LOGGER = LoggerFactory
			.getLogger(AbstractHibernateTestDb.class);

	@BeforeClass
	public static void setUpHibernate() throws Exception {
		// A SessionFactory is set up once for an application
		URL url = AbstractHibernateTestDb.class
				.getResource("/hibernate-in-mem.cfg.xml");
		sessionFactory = new Configuration().configure(url)
				.buildSessionFactory();
	}

	@AfterClass
	public static void closeHibernate() {
		if (sessionFactory != null) {
			sessionFactory.close();
		}
	}

	@Before
	public void setUpTransaction() throws Exception {
		sessionFactory.getCurrentSession().beginTransaction();
	}

	@After
	public void cleanRollback() throws Exception {
		sessionFactory.getCurrentSession().getTransaction().rollback();
	}

	protected void printNewspapers() {
		@SuppressWarnings("unchecked")
		List<Newspaper> newspaper = sessionFactory.getCurrentSession()
				.createCriteria(Newspaper.class).list();

		LOGGER.trace("{}", newspaper);
	}

	protected void printIssues() {
		@SuppressWarnings("unchecked")
		List<Issue> issues = sessionFactory.getCurrentSession()
				.createCriteria(Issue.class).list();

		LOGGER.trace("{}", issues);
	}

	protected void printWords() {
		@SuppressWarnings("unchecked")
		List<Word> words = sessionFactory.getCurrentSession()
				.createCriteria(Word.class).list();

		LOGGER.trace("{}", words);
	}

}
