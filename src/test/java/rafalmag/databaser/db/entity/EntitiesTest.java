package rafalmag.databaser.db.entity;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import java.util.Date;
import java.util.List;

import org.hamcrest.Matchers;
import org.hibernate.Session;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.Test;

import rafalmag.databaser.db.AbstractHibernateTestDb;

public class EntitiesTest extends AbstractHibernateTestDb {

	@Test
	public void should_save_newspaper() {
		// given
		String title = "news";

		// when
		Session session = sessionFactory.getCurrentSession();

		Newspaper newspaper = new Newspaper(title);
		session.save(newspaper);

		@SuppressWarnings("unchecked")
		List<Newspaper> newspapers = session.createCriteria(Newspaper.class)
				.list();
		// then
		assertThat(newspapers, hasItem(new Newspaper(title)));
		assertThat(newspapers.get(0).getTitle(), is(title));
	}

	@Test
	public void should_save_word() {
		// given
		String title = "news";
		String wordString = "test";

		// when
		Session session = sessionFactory.getCurrentSession();

		Newspaper newspaper = new Newspaper(title);
		session.save(newspaper);

		Issue issue = new Issue(newspaper, new Date());
		session.save(issue);

		Word word = new Word(issue, wordString);
		session.save(word);

		// session = sessionFactory.getCurrentSession();

		@SuppressWarnings("unchecked")
		List<Word> result = sessionFactory.getCurrentSession()
				.createCriteria(Word.class).list();
		// then
		assertThat(result, hasItem(new Word(issue, wordString)));
		assertThat(result.get(0).getIssue().getNewspaper().getTitle(),
				is(title));
		assertThat(result.get(0).getWord(), is(wordString));
	}

	@Test
	public void should_save_word_cascade() {
		// given
		String title = "news";
		String wordString = "test";

		// when
		Session session = sessionFactory.getCurrentSession();

		Word word = new Word(new Issue(new Newspaper(title), new Date()),
				wordString);
		session.save(word);

		session = sessionFactory.getCurrentSession();

		@SuppressWarnings("unchecked")
		List<Word> result = session.createCriteria(Word.class).list();
		// then
		assertThat(result.get(0).getIssue().getNewspaper().getTitle(),
				is(title));
		assertThat(result.get(0).getWord(), is(wordString));
	}

	@SuppressWarnings("unchecked")
	@Test
	// (expected = ConstraintViolationException.class)
	public void should_each_newspaper_title_be_unique() {
		// check
		Session session = sessionFactory.getCurrentSession();
		List<Newspaper> newspaper = session.createCriteria(Newspaper.class)
				.list();
		assertThat(newspaper.size(), is(0));

		// when
		should_save_newspaper();
		try {
			should_save_newspaper(); // should throw
										// ConstraintViolationException
			fail("should be ex");
		} catch (ConstraintViolationException e) {
			// ok
		}

		// then
		newspaper = session.createCriteria(Newspaper.class).list();
		assertThat(newspaper.size(), is(1));
		printNewspapers();
	}

	@Test
	public void should_save_WordOccurrences() throws Exception {
		// given
		String title = "news";
		Session session = sessionFactory.getCurrentSession();

		Newspaper newspaper = new Newspaper(title);
		session.save(newspaper);

		String word = "word";
		long occurrences = 1L;

		// when
		WordOccurrences wo = new WordOccurrences();
		wo.setWord(word);
		wo.addOccurrences(newspaper, occurrences);
		session.save(wo);

		// then
		@SuppressWarnings("unchecked")
		List<WordOccurrences> wordOccurrencesList = session.createCriteria(
				WordOccurrences.class).list();
		// then
		assertThat(wordOccurrencesList, hasItem(wo));
		assertThat(wordOccurrencesList.get(0).getWord(), is(word));

		assertThat(
				wordOccurrencesList.get(0).getOccurrencesFor(
						new Newspaper(title)), equalTo(occurrences));
		assertThat(wordOccurrencesList.get(0).getBaseForm(),
				Matchers.nullValue());
		assertThat(wordOccurrencesList.get(0).getOtherForms(),
				Matchers.hasSize(0));

	}

	@Test
	public void should_save_WordOccurrences_with_duplicates() throws Exception {
		// given
		String title = "news";
		Session session = sessionFactory.getCurrentSession();

		Newspaper newspaper = new Newspaper(title);
		session.save(newspaper);

		String word_root = "word";
		long root_occurrences = 1L;

		String word_dup = "word2";
		long dup_occurrences = 3L;

		// when

		WordOccurrences wo_root = new WordOccurrences();
		wo_root.setWord(word_root);
		wo_root.addOccurrences(newspaper, root_occurrences);
		session.save(wo_root);

		WordOccurrences wo_dup = new WordOccurrences();
		wo_dup.setWord(word_dup);
		wo_dup.addOccurrences(newspaper, dup_occurrences);

		wo_dup.setBaseForm(wo_root);

		session.save(wo_dup);

		// then
		@SuppressWarnings("unchecked")
		List<WordOccurrences> wordOccurrencesList = session.createCriteria(
				WordOccurrences.class).list();
		// then
		assertThat(wordOccurrencesList, hasItem(wo_root));
		assertThat(wordOccurrencesList, hasItem(wo_dup));

		WordOccurrences actual_wo_root;
		WordOccurrences actual_wo_dup;
		if (wordOccurrencesList.get(0).equals(wo_root)) {
			actual_wo_root = wordOccurrencesList.get(0);
			actual_wo_dup = wordOccurrencesList.get(1);
		} else {
			actual_wo_root = wordOccurrencesList.get(1);
			actual_wo_dup = wordOccurrencesList.get(0);
		}

		assertThat(actual_wo_dup.getWord(), is(word_dup));

		assertThat(actual_wo_dup.getOccurrencesFor(new Newspaper(title)),
				equalTo(dup_occurrences));
		assertThat(actual_wo_dup.getBaseForm(), is(actual_wo_root));

		assertThat(actual_wo_root.getWord(), is(word_root));
		assertThat(actual_wo_root.getOccurrencesFor(new Newspaper(title)),
				equalTo(root_occurrences + dup_occurrences));
		assertThat(actual_wo_root.getOtherForms(),
				Matchers.hasItem(actual_wo_dup));
	}
}
