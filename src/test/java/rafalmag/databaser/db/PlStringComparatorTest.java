package rafalmag.databaser.db;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.lessThan;
import static org.junit.Assert.assertThat;

import java.util.Comparator;

import org.junit.Test;

public class PlStringComparatorTest {

	@Test
	public void should_cmp_using_collator() throws Exception {
		// given

		// when
		PlStringComparator comparator = new PlStringComparator();
		Comparator<Object> plCollator = comparator.getComparator();

		// then
		assertThat(plCollator.compare("a", "a"), equalTo(0));
		assertThat(plCollator.compare("A", "a"), equalTo(0));
		assertThat(plCollator.compare("a", "ą"), lessThan(0));
		assertThat(plCollator.compare("ą", "b"), lessThan(0));
		assertThat(plCollator.compare("C", "Ć"), lessThan(0));
		assertThat(plCollator.compare("ć", "D"), lessThan(0));
		assertThat(plCollator.compare("z", "ź"), lessThan(0));
		assertThat(plCollator.compare("ź", "ż"), lessThan(0));
		assertThat(plCollator.compare("ala", "Ala"), equalTo(0));
		assertThat(plCollator.compare("Al ", "Ala"), lessThan(0));
		// bug track
		assertThat(plCollator.compare("l", "ł"), lessThan(0));
		assertThat(plCollator.compare("ł", "m"), lessThan(0));
		assertThat(plCollator.compare("cukrzyk", "ćwiczenia"), lessThan(0));
		assertThat(plCollator.compare("cytryny", "ćwiczenia"), lessThan(0));
		assertThat(plCollator.compare("zdziwić", "że"), lessThan(0));
		assertThat(plCollator.compare("zebrać", "że"), lessThan(0));
		assertThat(plCollator.compare("łapy", "zły"), lessThan(0));
		// TODO

	}
}
