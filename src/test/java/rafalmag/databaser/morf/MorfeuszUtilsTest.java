package rafalmag.databaser.morf;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import com.dawidweiss.morfeusz.InterpMorf;

public class MorfeuszUtilsTest extends MorfUtilsTest {

	private final MorfeuszUtils morfeuszUtils = new MorfeuszUtils();

	@Override
	protected MorfeuszUtils getMorfUtils() {
		return morfeuszUtils;
	}

	@Test
	public void should_get_lemmas_array() throws Exception {
		// given
		String input = "ala";
		// when
		InterpMorf[] analysis = morfeuszUtils.getLemmas(input);

		// then
		assertThat(Arrays.asList(analysis), hasSize(3));
		assertThat(analysis[0].getLemmaImage(), equalTo("Al"));
		assertThat(analysis[1].getLemmaImage(), equalTo("Ala"));
		assertThat(analysis[2].getLemmaImage(), equalTo("Alo"));
	}

	@Test
	public void should_convert_array_to_lemma_list() throws Exception {
		// given
		String input = "ala";
		InterpMorf[] analysis = morfeuszUtils.getLemmas(input);

		// when
		List<String> lemmaList = morfeuszUtils.toLemmaList(analysis);

		// then
		assertThat(lemmaList, hasItems("Al", "Ala", "Alo"));
	}

}
