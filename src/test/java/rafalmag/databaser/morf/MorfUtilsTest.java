package rafalmag.databaser.morf;

import static junitparams.JUnitParamsRunner.$;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.equalToIgnoringCase;
import static org.junit.Assert.assertThat;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(JUnitParamsRunner.class)
public abstract class MorfUtilsTest {

	protected abstract MorfUtils getMorfUtils();

	@Parameters({ "sdjkhfkkdkushfkdskdgfhgfhsgywergjb",
			"AAsdjkhfkkdkushfkdskdgfhgfhsgywergjb" })
	@Test
	public void should_get_lemma_for_unknow_word(String input) throws Exception {
		// given

		// when
		String lemma = getMorfUtils().getLemma(input);

		// then
		assertThat(lemma, equalToIgnoringCase(input));
	}

	@Parameters(method = "provideLemmaAndExpectedValue")
	@Test
	public void should_get_lemma(String input, String expected)
			throws Exception {
		// given

		// when
		String lemma = getMorfUtils().getLemma(input);

		// then
		assertThat(lemma, equalTo(expected));
	}

	protected Object[] provideLemmaAndExpectedValue() {
		return $($("abbasa", "abbasa"), $("abbasem", "abbasem"), // nothing
																	// found
				$("abbas", "abbas"), $("ala", "al"));
	}

	@Test(expected = IllegalArgumentException.class)
	public void should_get_lemma_throw_IAE_for_empty_input() throws Exception {
		// given
		String input = "";

		// when
		getMorfUtils().getLemma(input);

		// then ex
	}

	@Parameters({ "DOM", "dom", "Dom" })
	@Test
	public void should_get_the_same_lemma_ignoring_case(String input)
			throws Exception {
		// given

		// when
		String lemma = getMorfUtils().getLemma(input);

		// then
		assertThat(lemma, equalTo("dom"));
	}

}
