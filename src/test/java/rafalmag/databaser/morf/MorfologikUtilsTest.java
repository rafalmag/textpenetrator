package rafalmag.databaser.morf;

import static junitparams.JUnitParamsRunner.$;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import junitparams.Parameters;

import org.junit.Test;

public class MorfologikUtilsTest extends MorfUtilsTest {

	private final MorfologikUtils morfologikUtils = new MorfologikUtils();

	@Override
	protected MorfologikUtils getMorfUtils() {
		return morfologikUtils;
	}

	@Parameters(method = "provideCaseSensitiveLemmaAndExpectedValue")
	@Test
	public void should_get_lemma_be_case_sensitive(String input, String expected)
			throws Exception {
		// given

		// when
		String lemma = getMorfUtils().getLemma(input);

		// then
		assertThat(lemma, equalTo(expected));
	}

	protected Object[] provideCaseSensitiveLemmaAndExpectedValue() {
		return $($("ala", "al"), $("Ala", "ala"));
	}

	@Test
	public void should_return_instrumentalis() throws Exception {
		// given
		String input = "I";

		// when
		String lemma = getMorfUtils().getLemma(input);

		// then
		assertThat(lemma, equalTo("instrumentalis"));
	}

}
