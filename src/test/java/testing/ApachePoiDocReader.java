package testing;

import java.io.File;
import java.io.FileInputStream;
import java.net.URL;

import org.apache.poi.hwpf.extractor.WordExtractor;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ApachePoiDocReader {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(ApachePoiDocReader.class);

	@Test
	public void should_read_doc() throws Exception {
		URL resource = ApachePoiDocReader.class
				.getResource("/Gazeta Wyborcza/20100309.doc");
		File file = new File(resource.toURI());

		try (FileInputStream fis = new FileInputStream(file.getAbsolutePath())) {
			// HWPFDocument document = new HWPFDocument(fis);

			// document.getDocument();

			WordExtractor extractor = new WordExtractor(fis);
			String fileData = extractor.getTextFromPieces();
			LOGGER.debug("{}", fileData);
		}

	}
}