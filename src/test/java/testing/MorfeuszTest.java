package testing;

import static junit.framework.Assert.assertTrue;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import java.util.Arrays;

import org.junit.Ignore;
import org.junit.Test;

import com.dawidweiss.morfeusz.Analyzer;
import com.dawidweiss.morfeusz.InterpMorf;
import com.dawidweiss.morfeusz.Morfeusz;

public class MorfeuszTest {

	@Test
	public void should_get_about() throws Exception {
		// given
		Morfeusz morfeusz = Morfeusz.getInstance();
		// when
		String about = morfeusz.about();
		// then
		assertThat(about, containsString("Morfeusz SGJP"));
		assertThat(about, containsString("Dane lingwistyczne"));
		assertThat(about, containsString("Java JNI binding code"));
	}

	@Test
	public void should_get_lemma() throws Exception {
		// given
		String input = "Ala";
		Morfeusz instance = Morfeusz.getInstance();

		// when
		Analyzer analyzer = instance.getAnalyzer();
		InterpMorf[] analysis = analyzer.analyze(input);

		// then
		assertTrue(analyzer.getTokensNumber() == analysis.length);
		assertThat(analysis[0].getLemmaImage(), equalTo("Al"));
		assertThat(analysis[1].getLemmaImage(), equalTo("Ala"));
		assertThat(analysis[2].getLemmaImage(), equalTo("Alo"));
	}

	@Ignore
	// does not work...
	@Test
	public void should_get_lemma_for_negative_adj() throws Exception {
		// given
		String input = "nieduża";
		Morfeusz instance = Morfeusz.getInstance();

		// when
		Analyzer analyzer = instance.getAnalyzer();
		InterpMorf[] analysis = analyzer.analyze(input);
		System.out.println(Arrays.toString(analysis));

		// then
		assertThat(analysis[0].getLemmaImage(), equalTo("duży"));
	}

}
