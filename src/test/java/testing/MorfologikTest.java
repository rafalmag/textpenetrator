package testing;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import java.util.List;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import morfologik.stemming.PolishStemmer;
import morfologik.stemming.PolishStemmer.DICTIONARY;
import morfologik.stemming.WordData;

import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(JUnitParamsRunner.class)
public class MorfologikTest {

	@Parameters({ "Ala,Ala", "nieduża,duży", "Polski,Polski", "polski,polski",
			"polskich,polski", "Polsko,Polska", "polsko,polsko" })
	@Test
	public void should_get_lemma(String input, String expectedOutput)
			throws Exception {
		// given
		PolishStemmer polishStemmer = new PolishStemmer(DICTIONARY.COMBINED);

		// when
		List<WordData> lookup = polishStemmer.lookup(input);

		// then
		assertThat(lookup.get(0).getStem().toString(), equalTo(expectedOutput));
	}

}
