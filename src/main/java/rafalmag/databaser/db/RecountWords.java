package rafalmag.databaser.db;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import rafalmag.databaser.db.analyzer.Words;
import rafalmag.databaser.db.entity.WordOccurrences;
import rafalmag.databaser.morf.MorfUtils;

public class RecountWords {

	private final static Logger LOGGER = LoggerFactory
			.getLogger(RecountWords.class);

	private final Session session;

	private final MorfUtils morfUtils;

	public RecountWords(Session session, MorfUtils morfUtils) {
		this.session = session;
		this.morfUtils = morfUtils;
	}

	public void recountWords() {
		try {
			Double startTimeSec = System.currentTimeMillis() / 1000.0;
			purgeWO();
			Double seconds = System.currentTimeMillis() / 1000.0 - startTimeSec;
			LOGGER.info(
					"WordOccurrences table purged in {}s. recountGetWordsCountsObjects",
					seconds);
			startTimeSec = System.currentTimeMillis() / 1000.0;
			Words.recountGetWordsCountsObjects(session, morfUtils);
			seconds = System.currentTimeMillis() / 1000.0 - startTimeSec;
			LOGGER.info("recountGetWordsCountsObjects finished in {}s", seconds);
		} catch (HibernateException e) {
			LOGGER.error("Could not recountGetWordsCountsObjects because of "
					+ e.getMessage(), e);
		}
	}

	protected void purgeWO() {
		for (Object wo : session.createCriteria(WordOccurrences.class).list()) {
			LOGGER.debug("deleting {}", wo);
			session.delete(wo);
		}
		LOGGER.info("Flushing changes...");
		session.flush();
		LOGGER.debug("Flushing done.");
	}
}
