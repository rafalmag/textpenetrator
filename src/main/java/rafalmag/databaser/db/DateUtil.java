package rafalmag.databaser.db;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {

	private final static SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat(
			"yyyyddMM");

	public static Date toDate(String dateString) throws ParseException {
		return DATE_FORMATTER.parse(dateString);
	}

	public static String toString(Date date) {
		return DATE_FORMATTER.format(date);
	}
}
