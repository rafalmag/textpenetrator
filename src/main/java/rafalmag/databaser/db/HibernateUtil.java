package rafalmag.databaser.db;

import java.net.URL;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HibernateUtil {

	private final static Logger LOGGER = LoggerFactory
			.getLogger(HibernateUtil.class);

	private static final SessionFactory sessionFactory = buildSessionFactory();

	private static SessionFactory buildSessionFactory() {
		try {
			URL url = HibernateUtil.class.getResource("/hibernate.cfg.xml");
			return new Configuration().configure(url).buildSessionFactory();
		} catch (Throwable ex) {
			LOGGER.error(
					"Initialization of SessionFactory failed."
							+ ex.getMessage(), ex);
			throw new ExceptionInInitializerError(ex);
		}
	}

	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}

}