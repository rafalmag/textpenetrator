package rafalmag.databaser.db.analyzer;

import java.util.List;
import java.util.Map;

import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import rafalmag.databaser.db.DateUtil;
import rafalmag.databaser.db.entity.Newspaper;
import rafalmag.databaser.db.entity.Word;
import rafalmag.databaser.db.entity.WordOccurrences;
import rafalmag.databaser.morf.MorfUtils;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

public class Words {

	private static final Logger LOGGER = LoggerFactory.getLogger(Words.class);

	/**
	 * Return list like: [["word","gw","nd"],["abc","2","3"],["abcd","3",3"]]
	 * 
	 * @param session
	 * @return
	 */
	public static List<List<String>> getWordCountsAsLists(Session session) {
		WordCounts wordCounts = getWordsCounts(session);
		return getWordCountsAsLists(wordCounts);
	}

	/**
	 * Return list like: [["word","gw","nd"],["abc","2","3"],["abcd","3",3"]]
	 * 
	 * @param session
	 * @return
	 */
	public static List<List<String>> getWordCountsAsLists(WordCounts wordCounts) {
		List<List<String>> rows = Lists.newArrayList();
		rows.add(wordCounts.getHeader());
		rows.addAll(wordCounts.getWordCountsList());
		return rows;
	}

	public static WordCounts getWordsCounts(Session session) {
		LOGGER.debug("getWordCounts (total)");
		WordCountsCreator wordCounts = new WordCountsCreator();

		@SuppressWarnings("unchecked")
		List<Newspaper> newspapers = session.createCriteria(Newspaper.class)
				.list();

		LOGGER.debug("Got newspapers from database: {}", newspapers.size());
		@SuppressWarnings("unchecked")
		List<WordOccurrences> wordOccurrencesList = session
				.createCriteria(WordOccurrences.class)
				.add(Restrictions.isNull("baseForm")).list();
		LOGGER.info("Got WO from database: {}", wordOccurrencesList.size());
		for (Newspaper newspaper : newspapers) {
			for (WordOccurrences wo : wordOccurrencesList) {
				wordCounts.putWord(wo.getWord(),
						wo.getOccurrencesFor(newspaper), newspaper.getTitle());
			}
		}

		return new WordCounts(wordCounts);
	}

	public static List<List<String>> getWords(Session session) {
		LOGGER.debug("getWords");
		@SuppressWarnings("unchecked")
		List<Word> list = session.createQuery("from Word").list();
		List<List<String>> result = Lists.newArrayListWithCapacity(list.size());
		for (Word w : list) {
			List<String> entry = Lists.newArrayListWithCapacity(3);
			entry.add(w.getIssue().getNewspaper().getTitle());
			entry.add(DateUtil.toString(w.getIssue().getDate()));
			entry.add(w.getWord());
			result.add(entry);
		}
		return result;
	}

	/**
	 * It uses Morfeusz.
	 * 
	 * @param session
	 */
	public static void recountGetWordsCountsObjects(Session session,
			MorfUtils morfUtils) {
		@SuppressWarnings("unchecked")
		List<Newspaper> newspapers = session.createCriteria(Newspaper.class)
				.list();
		Map<String /* word */, WordOccurrences> wordOccurrencesMap = Maps
				.newHashMap();
		for (Newspaper newspaper : newspapers) {
			LOGGER.info("Counting for newspaper: {}", newspaper);
			@SuppressWarnings("unchecked")
			List<Object[]> list = session
					.createCriteria(Word.class, "w")
					.createCriteria("w.issue", "i")
					// .createCriteria("i.newspaper", "n")
					.add(Restrictions.eq("i.newspaper", newspaper))
					.setProjection(
							Projections.projectionList()
									.add(Projections.groupProperty("w.word"))
									.add(Projections.rowCount(), "count"))
					.list();

			for (Object[] wordRow : list) {
				String word = (String) wordRow[0];
				Long occurrences = (Long) wordRow[1];
				word = morfUtils.getLemma(word);
				updateWordOccurrencesMap(wordOccurrencesMap, newspaper, word,
						occurrences);
			}
		}
		LOGGER.info("Counting done, saving wordOccurrencesMap:"
				+ wordOccurrencesMap.size());
		for (WordOccurrences wordOccurrences : wordOccurrencesMap.values()) {
			session.save(wordOccurrences);
		}
		LOGGER.info("Counting done, saving done");
	}

	protected static void updateWordOccurrencesMap(
			Map<String /* word */, WordOccurrences> wordOccurrencesMap,
			Newspaper newspaper, String word, Long occurrences) {
		WordOccurrences wordOccurrences = wordOccurrencesMap.get(word);
		if (wordOccurrences == null) {
			wordOccurrences = new WordOccurrences();
			wordOccurrencesMap.put(word, wordOccurrences);
		}
		wordOccurrences.setWord(word);
		wordOccurrences.addOccurrences(newspaper, occurrences);
	}

}
