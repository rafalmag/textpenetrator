package rafalmag.databaser.db.analyzer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import au.com.bytecode.opencsv.CSVWriter;

public class ToCsv {

	public static final Charset CHARSET = Charset.forName("windows-1250");
	private static final Logger LOGGER = LoggerFactory.getLogger(ToCsv.class);

	public static void save(File file, List<List<String>> rows)
			throws IOException {
		LOGGER.debug("Saving " + rows.size() + " rows to " + file);
		Double startSec = System.currentTimeMillis() / 1000.0;
		try (CSVWriter writer = new CSVWriter(new OutputStreamWriter(
				new FileOutputStream(file, false), CHARSET), ';')) {
			for (List<String> columnsForRow : rows) {
				String[] arrayToStore = columnsForRow
						.toArray(new String[columnsForRow.size()]);
				writer.writeNext(arrayToStore);
			}
		}
		Double seconds = System.currentTimeMillis() / 1000.0 - startSec;
		LOGGER.debug("Saving done in {}s", seconds);
	}
}
