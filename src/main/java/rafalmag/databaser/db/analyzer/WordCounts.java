package rafalmag.databaser.db.analyzer;

import java.util.List;

import com.google.common.collect.Lists;

public class WordCounts {

	private final List<String> header;
	private final List<List<String>> wordCountsList;

	public WordCounts(WordCountsCreator wordCounts) {
		header = wordCounts.getHeader();
		wordCountsList = wordCounts.getWordCountsList();
	}

	public WordCounts() {
		header = Lists.newArrayListWithCapacity(0);
		wordCountsList = Lists.newArrayListWithCapacity(0);
	}

	public List<String> getHeader() {
		return header;
	}

	public List<List<String>> getWordCountsList() {
		return wordCountsList;
	}

}
