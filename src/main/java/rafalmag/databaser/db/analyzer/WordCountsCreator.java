package rafalmag.databaser.db.analyzer;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.google.common.base.Objects;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

class WordCountsCreator {

	// key - word
	// value - map of newspaper -> occurrences
	private final Map<String, Map<String, Long>> wordsMap = Maps.newHashMap();

	public List<String> getHeader() {
		Set<String> set = Sets.newTreeSet();
		for (Map<String, Long> occurrencesMap : wordsMap.values()) {
			set.addAll(occurrencesMap.keySet());
		}
		List<String> header = Lists.newArrayList();
		header.add("word");
		header.addAll(set);
		return Collections.unmodifiableList(header);
	}

	public List<List<String>> getWordCountsList() {
		List<String> header = getHeader();
		List<List<String>> wordCountsList = Lists.newArrayList();
		for (Entry<String, Map<String, Long>> wordOccurrences : wordsMap
				.entrySet()) {
			List<String> wordCountItem = Lists.newArrayListWithCapacity(header
					.size());
			wordCountItem.add(wordOccurrences.getKey());
			for (int i = 1; i < header.size(); i++) {
				String newspaperName = header.get(i);
				Long occurrencesForNewspaper = wordOccurrences.getValue().get(
						newspaperName);
				Long itemToAdd = Objects.firstNonNull(occurrencesForNewspaper,
						0L);
				wordCountItem.add(itemToAdd.toString());
			}
			wordCountsList.add(wordCountItem);
		}
		return wordCountsList;
	}

	public void putWord(String word, Long occurancesInNewspaper,
			String newspaper) {
		Map<String, Long> oldOccurrencesMap = wordsMap.get(word);
		if (oldOccurrencesMap == null) {
			Map<String, Long> newOccurrencesMap = Maps.newHashMap();
			newOccurrencesMap.put(newspaper, occurancesInNewspaper);
			wordsMap.put(word, newOccurrencesMap);
		} else {
			oldOccurrencesMap.put(newspaper, occurancesInNewspaper);
		}
	}
}
