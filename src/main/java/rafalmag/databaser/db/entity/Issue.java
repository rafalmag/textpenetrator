package rafalmag.databaser.db.entity;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.GenericGenerator;

import com.google.common.collect.Lists;

@SuppressWarnings("serial")
@Entity
public class Issue implements Serializable {

	private static final SimpleDateFormat SIMPLE_DATE_FORMAT_FOR_TO_STRING = new SimpleDateFormat(
			"yyyy-MM-dd");

	@Id
	@GeneratedValue(generator = "increment")
	@GenericGenerator(name = "increment", strategy = "increment")
	private Long id;

	@Cascade({ CascadeType.ALL })
	@ManyToOne
	// (cascade = { javax.persistence.CascadeType.ALL }, optional = false)
	@Basic(optional = false)
	// @OnDelete(action = OnDeleteAction.CASCADE)
	private Newspaper newspaper;

	@OneToMany(orphanRemoval = true, mappedBy = "issue")
	// @JoinColumn(name = "issue")
	// @OnDelete(action = OnDeleteAction.CASCADE)
	private final List<Word> words;

	public List<Word> getWords() {
		return words;
	}

	public void setWords(List<Word> words) {
		this.words.clear();
		this.words.addAll(words);
	}

	@Temporal(TemporalType.DATE)
	private Date date;

	protected Issue() {
		// this form used by Hibernate
		this(null, null);
	}

	public Issue(Newspaper newspaper, Date date) {
		this.newspaper = newspaper;
		this.date = date;
		this.words = Lists.newArrayList();
	}

	public Long getId() {
		return id;
	}

	protected void setId(Long id) {
		this.id = id;
	}

	public Newspaper getNewspaper() {
		return newspaper;
	}

	public void setNewspaper(Newspaper newspaper) {
		this.newspaper = newspaper;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "Issue [id=" + id + ", newspaper=" + newspaper + ", date="
				+ SIMPLE_DATE_FORMAT_FOR_TO_STRING.format(date) + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result
				+ ((newspaper == null) ? 0 : newspaper.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Issue other = (Issue) obj;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (newspaper == null) {
			if (other.newspaper != null)
				return false;
		} else if (!newspaper.equals(other.newspaper))
			return false;
		return true;
	}

}
