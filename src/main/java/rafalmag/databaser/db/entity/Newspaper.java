package rafalmag.databaser.db.entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.GenericGenerator;

import com.google.common.collect.Sets;

@SuppressWarnings("serial")
@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "title" }))
public class Newspaper implements Serializable {
	@Id
	@GeneratedValue(generator = "increment")
	@GenericGenerator(name = "increment", strategy = "increment")
	private Long id;

	@Basic(optional = false)
	private String title;

	@OneToMany(orphanRemoval = true, mappedBy = "newspaper")
	// @JoinColumn(name = "newspaper")
	// @OnDelete(action = OnDeleteAction.CASCADE)
	private final Set<Issue> issues;

	protected Newspaper() {
		// this form used by Hibernate
		this(null);
	}

	public Newspaper(String title) {
		this.title = title;
		issues = Sets.newHashSet();
	}

	public Long getId() {
		return id;
	}

	// was protected
	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Override
	public String toString() {
		return "Newspaper [id=" + id + ", title=" + title + "]";
	}

	public Collection<Issue> getIssues() {
		return issues;
	}

	public void setIssues(Set<Issue> issues) {
		this.issues.clear();
		this.issues.addAll(issues);
		// this.issues = issues;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Newspaper other = (Newspaper) obj;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}

}
