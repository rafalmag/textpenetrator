package rafalmag.databaser.db.entity;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedSet;

import javax.annotation.Nullable;
import javax.persistence.Basic;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Sort;
import org.hibernate.annotations.SortType;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

/**
 * Occurrences
 * 
 * @author rafalmag
 * 
 */
@Entity
@SuppressWarnings("serial")
public class WordOccurrences implements Serializable,
		Comparable<WordOccurrences> {

	@Id
	@GeneratedValue(generator = "increment")
	@GenericGenerator(name = "increment", strategy = "increment")
	private Long id;

	@Basic(optional = false)
	private String word;

	// @Cascade({ CascadeType.ALL })
	@ElementCollection(fetch = FetchType.EAGER)
	private final Map<Newspaper, Long> occurrences;

	// @Cascade({ CascadeType.ALL })
	@OneToMany(orphanRemoval = false, mappedBy = "baseForm", fetch = FetchType.EAGER)
	@Sort(type = SortType.NATURAL)
	private final SortedSet<WordOccurrences> otherForms;

	@Cascade({ CascadeType.ALL })
	@ManyToOne
	@JoinColumn(nullable = true)
	@Basic(optional = false)
	private WordOccurrences baseForm;

	public WordOccurrences() {
		occurrences = Maps.newHashMap();
		otherForms = Sets.newTreeSet();
	}

	//
	public String getWord() {
		return word;
	}

	public void setWord(String word) {
		this.word = word;
	}

	private Map<Newspaper, Long> getOccurrences() {
		return occurrences;
	}

	private void setOccurrences(Map<Newspaper, Long> occurrences) {
		this.occurrences.clear();
		this.occurrences.putAll(occurrences);
		// this.occurrences = occurrences;
	}

	@Nullable
	public WordOccurrences getBaseForm() {
		return baseForm;
	}

	/**
	 * It will set otherForms in baseForm (if not null) as well and update
	 * occurrences. Beware ConcurrentModificationException !
	 * 
	 * @param baseForm
	 */
	public void setBaseForm(WordOccurrences baseForm) {
		if (this.baseForm != null) {
			// fix old object

			// fix otherForms
			this.baseForm.otherForms.remove(this);

			// fix occurrences
			Map<Newspaper, Long> oldParentOccurances = this.baseForm
					.getOccurrences();
			for (Entry<Newspaper, Long> entry : occurrences.entrySet()) {
				Long oldParentOccurrences = oldParentOccurances.get(entry
						.getKey());
				if (oldParentOccurances != null) {
					Long newValue = oldParentOccurrences - entry.getValue();
					if (newValue == 0) {
						oldParentOccurances.remove(entry.getKey());
					} else if (newValue > 0) {
						oldParentOccurances.put(entry.getKey(), newValue);
					} else {
						throw new IllegalStateException(
								"newValue cant be less than 0, this=" + this
										+ " baseForm=" + baseForm);
					}
				}
			}
		}
		// own baseForm
		this.baseForm = baseForm;
		if (baseForm != null) {
			// update new otherforms
			// their otherForms
			baseForm.otherForms.add(this);

			// their occurrences
			Map<Newspaper, Long> newParentOccurrencesMap = baseForm.occurrences;
			for (Entry<Newspaper, Long> entry : occurrences.entrySet()) {
				Long baseFormOccurrences = newParentOccurrencesMap.get(entry
						.getKey());
				if (baseFormOccurrences == null) {
					newParentOccurrencesMap.put(entry.getKey(),
							entry.getValue());
				} else {
					Long newValue = baseFormOccurrences + entry.getValue();
					newParentOccurrencesMap.put(entry.getKey(), newValue);
				}
			}
		}
	}

	public SortedSet<WordOccurrences> getOtherForms() {
		return otherForms;
	}

	/**
	 * Do not use it outside. Use {@link #setBaseForm(WordOccurrences)} instead.
	 * 
	 * @param otherForms
	 */
	protected void setOtherForms(SortedSet<WordOccurrences> otherForms) {
		this.otherForms.clear();
		this.otherForms.addAll(otherForms);

		// this.otherForms = otherForms;

		// if (otherForms instanceof SortedSet<?>) {
		// this.otherForms = (SortedSet<WordOccurrences>) otherForms;
		// } else {
		// this.otherForms = Sets.newTreeSet(otherForms);
		// }
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((baseForm == null) ? 0 : baseForm.hashCode());
		result = prime * result
				+ ((occurrences == null) ? 0 : occurrences.hashCode());
		result = prime * result + ((word == null) ? 0 : word.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WordOccurrences other = (WordOccurrences) obj;
		if (baseForm == null) {
			if (other.baseForm != null)
				return false;
		} else if (!baseForm.equals(other.baseForm))
			return false;
		if (occurrences == null) {
			if (other.occurrences != null)
				return false;
		} else if (!occurrences.equals(other.occurrences))
			return false;
		if (word == null) {
			if (other.word != null)
				return false;
		} else if (!word.equals(other.word))
			return false;
		return true;
	}

	@Override
	public int compareTo(WordOccurrences o) {
		return word.compareTo(o.getWord());
	}

	@Override
	public String toString() {
		return "WordOccurrences [word=" + word + ", occurrences=" + occurrences
				+ ", otherForms=" + otherForms.size() + ", baseForm="
				+ (baseForm == null ? "null" : baseForm.getWord()) + "]";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getOccurrencesFor(Newspaper newspaper) {
		return getOccurrences().get(newspaper);
	}

	public static String countWords(
			List<WordOccurrences> rootWordOccurrencesList) {
		Map<Newspaper, Long> count = Maps.newHashMap();
		for (WordOccurrences wo : rootWordOccurrencesList) {
			Map<Newspaper, Long> occurrences = wo.getOccurrences();
			for (Entry<Newspaper, Long> entry : occurrences.entrySet()) {
				Long value = count.get(entry.getKey());
				if (value == null) {
					count.put(entry.getKey(), entry.getValue());
				} else {
					Long newValue = value + entry.getValue();
					count.put(entry.getKey(), newValue);
				}
			}
		}
		return count.toString();
	}

	public void addOccurrences(Newspaper newspaper, Long occurrencesToAdd) {
		Long oldOccurrences = occurrences.get(newspaper);
		Long occurrencesToPut;
		if (oldOccurrences != null) {
			occurrencesToPut = occurrencesToAdd + oldOccurrences;
		} else {
			occurrencesToPut = occurrencesToAdd;
		}
		occurrences.put(newspaper, occurrencesToPut);

	}

}
