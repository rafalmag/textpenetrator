package rafalmag.databaser.db;

import java.text.Collator;
import java.util.Comparator;
import java.util.Locale;

public class PlStringComparator {

	private final Collator plCollator;

	public PlStringComparator() {
		Collator plCollator = Collator.getInstance(Locale
				.forLanguageTag("pl-PL"));
		plCollator.setStrength(Collator.SECONDARY);
		this.plCollator = plCollator;
	}

	public Comparator<Object> getComparator() {
		return plCollator;
	}
}
