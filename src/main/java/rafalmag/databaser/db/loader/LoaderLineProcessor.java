package rafalmag.databaser.db.loader;

import java.io.IOException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.hibernate.Session;

import rafalmag.databaser.db.entity.Issue;
import rafalmag.databaser.db.entity.Word;

import com.google.common.collect.Lists;

public class LoaderLineProcessor /* implements LineProcessor<Void> */{

	private static final Pattern PATTERN = Pattern
			.compile("[a-zA-ZąśżźćęńółĄŚŻŹĆĘŃÓŁ]+[a-zA-ZąśżźćęńółĄŚŻŹĆĘŃÓŁ0-9]*|[!?]");

	static List<String> getWords(String input) {
		List<String> list = Lists.newArrayList();
		Matcher matcher = PATTERN.matcher(input);
		while (matcher.find()) {
			list.add(matcher.group(0));
		}
		return list;
	}

	private final Session session;
	private final Issue issue;

	public LoaderLineProcessor(Session session, Issue issue) {
		this.session = session;
		this.issue = issue;
	}

	// @Override
	public boolean processLine(String line) throws IOException {

		List<String> words = getWords(line);
		for (String s : words) {
			session.save(new Word(issue, s));
		}

		return true;
	}

	// @Override
	// public Void getResult() {
	// return null; // void
	// }

}
