package rafalmag.databaser.db.loader;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.Date;

import org.apache.poi.hwpf.extractor.WordExtractor;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import rafalmag.databaser.db.DateUtil;
import rafalmag.databaser.db.entity.Issue;
import rafalmag.databaser.db.entity.Newspaper;

import com.google.common.base.Splitter;

public class FileLoader {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(FileLoader.class);

	private final Session session;

	public FileLoader(Session session) {
		this.session = session;
	}

	public void load(final File file, Newspaper newspaper) throws IOException,
			ParseException {

		String dateString = file.getName().replace(".doc", "");
		Date date = DateUtil.toDate(dateString);

		Issue issue = new Issue(newspaper, date);
		LOGGER.info("Loading new file " + file + " of newspaper issue " + issue);
		session.save(issue);

		LoaderLineProcessor loaderLineProcessor = new LoaderLineProcessor(
				session, issue);

		String fileData;
		try (FileInputStream fis = new FileInputStream(file.getAbsolutePath())) {
			WordExtractor extractor = new WordExtractor(fis);
			fileData = extractor.getTextFromPieces();
		}
		Iterable<String> lines = Splitter.on("\n").split(fileData);
		for (String line : lines) {
			loaderLineProcessor.processLine(line);
		}

		// InputSupplier<FileReader> supplier = new InputSupplier<FileReader>()
		// {
		//
		// @Override
		// public FileReader getInput() throws IOException {
		// return new FileReader(file);
		// }
		// };
		//
		// InputSupplier<FileReader> supplier = new InputSupplier<FileReader>()
		// {
		//
		// @Override
		// public FileReader getInput() throws IOException {
		// return new FileReader(file);
		// }
		// };
		//
		// CharStreams.readLines(supplier, loaderLineProcessor);
	}
}
