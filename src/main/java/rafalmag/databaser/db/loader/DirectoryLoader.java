package rafalmag.databaser.db.loader;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.ParseException;

import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import rafalmag.databaser.db.entity.Newspaper;

public class DirectoryLoader {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(DirectoryLoader.class);

	private FileLoader fileLoader;
	private final Session session;

	public DirectoryLoader(Session session) {
		this.session = session;
		fileLoader = new FileLoader(session);
	}

	void setFileLoader(FileLoader fileLoader) {
		this.fileLoader = fileLoader;
	}

	public void loadDirectory(File dir) throws IOException {
		String newspaperName = dir.getName();
		Newspaper newspaper = new Newspaper(newspaperName);

		boolean newspaperSaved = false;

		DirectoryStream<Path> newDirectoryStream = Files.newDirectoryStream(
				dir.toPath(), "*.doc");
		for (Path p : newDirectoryStream) {
			if (!newspaperSaved) {
				session.save(newspaper);
				newspaperSaved = true;
			}
			try {
				fileLoader.load(p.toFile(), newspaper);
			} catch (ParseException pe) {
				LOGGER.error(
						"Could not load file " + p + " because of "
								+ pe.getMessage(), pe);
			}
		}
	}
}
