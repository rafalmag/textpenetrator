package rafalmag.databaser.db;

import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import rafalmag.databaser.db.entity.Newspaper;
import rafalmag.databaser.db.entity.WordOccurrences;

public class DbPurger {

	private final static Logger LOGGER = LoggerFactory
			.getLogger(DbPurger.class);

	private final Session session;

	public DbPurger(Session session) {
		this.session = session;
	}

	public void purge() {
		LOGGER.info("DB purge");
		// session.createQuery("delete from WordOccurrences").executeUpdate();
		// session.createQuery("delete from Newspaper").executeUpdate();
		// session.createQuery("delete from Issue").executeUpdate();
		// session.createQuery("delete from Words").executeUpdate();
		for (Object wo : session.createCriteria(WordOccurrences.class).list()) {
			LOGGER.trace("deleting {}", wo);
			session.delete(wo);
		}
		LOGGER.debug("DB purged WordOccurrences. Before flushing...");
		session.flush();
		LOGGER.debug("After flushing... Deleting Newspapers...");
		for (Object newspaper : session.createCriteria(Newspaper.class).list()) {
			LOGGER.info("deleting {}", newspaper);
			// ((Newspaper)newspaper).getIssues()
			session.delete(newspaper);
		}
		LOGGER.debug("DB purged Newspapers. Before flushing...");
		session.flush();
		LOGGER.debug("After flushing...");
	}
}
