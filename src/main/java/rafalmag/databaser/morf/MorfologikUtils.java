package rafalmag.databaser.morf;

import java.util.List;

import morfologik.stemming.PolishStemmer;
import morfologik.stemming.PolishStemmer.DICTIONARY;
import morfologik.stemming.WordData;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Preconditions;

public class MorfologikUtils implements MorfUtils {

	private final static Logger LOGGER = LoggerFactory
			.getLogger(MorfologikUtils.class);

	private final PolishStemmer polishStemmer = new PolishStemmer(
			DICTIONARY.COMBINED);

	@Override
	public String getLemma(String input) {
		Preconditions.checkArgument(input != null);
		if (input.isEmpty()) {
			throw new IllegalArgumentException("getLamma input cannot be empty");
		}
		List<WordData> list = polishStemmer.lookup(input);
		if (list.size() == 0) {
			String inputLowerCase = input.toLowerCase();
			if (input.equals(inputLowerCase)) {
				LOGGER.debug("Unknow lemma for: {}", input);
				return input;
			} else {
				LOGGER.debug("Unknow lemma for: {}. Trying lowercase.", input);
				return getLemma(inputLowerCase);
			}
		}
		String lemma = list.get(0).getStem().toString();
		if (list.size() > 1) {
			LOGGER.debug(
					"Multiple lemmas for '{}' choosen '{}'; other possibilities: {}",
					new Object[] { input, lemma, list });
		}
		return lemma.toLowerCase();
	}

	@Override
	public String toString() {
		return "MorfologikUtils";
	}

}
