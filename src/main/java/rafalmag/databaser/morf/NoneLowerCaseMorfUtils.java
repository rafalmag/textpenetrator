package rafalmag.databaser.morf;

public class NoneLowerCaseMorfUtils implements MorfUtils {

	@Override
	public String getLemma(String input) {
		return input.toLowerCase();
	}

	@Override
	public String toString() {
		return "NoneLowerCaseMorfUtils";
	}

}
