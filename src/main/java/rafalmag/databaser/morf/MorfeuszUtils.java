package rafalmag.databaser.morf;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dawidweiss.morfeusz.Analyzer;
import com.dawidweiss.morfeusz.InterpMorf;
import com.dawidweiss.morfeusz.Morfeusz;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;

public class MorfeuszUtils implements MorfUtils {

	private final static Logger LOGGER = LoggerFactory
			.getLogger(MorfeuszUtils.class);

	private Morfeusz instance;

	public MorfeuszUtils() {
		try {
			instance = Morfeusz.getInstance();
		} catch (SecurityException | UnsatisfiedLinkError | IOException e) {
			throw new IllegalStateException("Could not get morfeusz instance",
					e);
		}
	}

	public InterpMorf[] getLemmas(String input) {
		Analyzer analyzer = instance.getAnalyzer();
		InterpMorf[] analysis = analyzer.analyze(input);
		return analysis;
	}

	public List<String> toLemmaList(InterpMorf[] analizis) {
		ArrayList<String> list = Lists
				.newArrayListWithCapacity(analizis.length);
		for (InterpMorf morf : analizis) {
			list.add(morf.getLemmaImage());
		}
		return list;
	}

	@Override
	public String getLemma(String input) {
		Preconditions.checkArgument(input != null);
		if (input.isEmpty()) {
			throw new IllegalArgumentException("getLamma input cannot be empty");
		}
		InterpMorf[] interpMorfs = getLemmas(input);
		if (interpMorfs.length == 0) {
			throw new IllegalStateException("Should not return empty list");
		}
		String lemma = interpMorfs[0].getLemmaImage();
		if (interpMorfs.length > 1) {
			LOGGER.debug(
					"Multiple lemmas for '{}' choosen '{}'; other possibilities: {}",
					new Object[] { input, lemma, Arrays.toString(interpMorfs) });
		}
		if (lemma.isEmpty()) {
			LOGGER.debug("Unknow lemma for: {}", input);
			return input.toLowerCase();
		} else {
			return lemma.toLowerCase();
		}
	}

	@Override
	public String toString() {
		return "MorfeuszUtils";
	}

}
