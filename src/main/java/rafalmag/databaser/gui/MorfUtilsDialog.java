package rafalmag.databaser.gui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import rafalmag.databaser.morf.MorfUtils;
import rafalmag.databaser.morf.MorfeuszUtils;
import rafalmag.databaser.morf.MorfologikUtils;
import rafalmag.databaser.morf.NoneLowerCaseMorfUtils;

public class MorfUtilsDialog extends Dialog {

	private final static Logger LOGGER = LoggerFactory
			.getLogger(MorfUtilsDialog.class);

	protected MorfUtils result = null;
	protected Shell shell;

	/**
	 * Create the dialog.
	 * 
	 * @param parent
	 * @param style
	 */
	public MorfUtilsDialog(Shell parent, int style) {
		super(parent, style);
		setText("SWT Dialog");
	}

	/**
	 * Open the dialog.
	 * 
	 * @return the result
	 */
	public MorfUtils open() {
		createContents();
		shell.open();
		shell.layout();
		Display display = getParent().getDisplay();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		LOGGER.info("Returning " + result);
		return result;
	}

	/**
	 * Create contents of the dialog.
	 */
	private void createContents() {
		shell = new Shell(getParent(), getStyle());
		// shell.setSize(450, 300);
		shell.setText(getText());
		RowLayout rl_shell = new RowLayout(SWT.VERTICAL);
		rl_shell.fill = true;
		shell.setLayout(rl_shell);

		Label lbLabel = new Label(shell, SWT.WRAP);
		lbLabel.setText("Please select library to merge words with the same lemma/steam.\nAll previous word occurrences data will be purged.");

		final Button btnMorfeusz = new Button(shell, SWT.RADIO);
		btnMorfeusz.setText("Morfeusz");

		final Button btnMorfologik = new Button(shell, SWT.RADIO);
		btnMorfologik.setText("Morfologik");

		final Button btnNone = new Button(shell, SWT.RADIO);
		btnNone.setText("None");
		btnNone.setSelection(true);

		Composite composite = new Composite(shell, SWT.NONE);
		RowLayout rl_composite = new RowLayout(SWT.HORIZONTAL);
		rl_composite.fill = true;
		rl_composite.justify = true;
		rl_composite.pack = false;
		composite.setLayout(rl_composite);

		Button btnOk = new Button(composite, SWT.NONE);
		btnOk.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				if (btnMorfeusz.getSelection()) {
					setResult(new MorfeuszUtils());
				}
				if (btnMorfologik.getSelection()) {
					setResult(new MorfologikUtils());
				}
				if (btnNone.getSelection()) {
					setResult(new NoneLowerCaseMorfUtils());
				}
				shell.dispose();
			}
		});
		btnOk.setText("Ok");

		Button btnCancel = new Button(composite, SWT.NONE);
		btnCancel.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				shell.dispose();
			}
		});
		btnCancel.setText("Cancel");
		shell.pack();
	}

	protected void setResult(MorfUtils result) {
		this.result = result;
	}
}
