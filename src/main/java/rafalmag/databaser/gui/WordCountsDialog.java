package rafalmag.databaser.gui;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.SortedSet;

import javax.annotation.Nullable;

import org.eclipse.jface.layout.TreeColumnLayout;
import org.eclipse.jface.viewers.ColumnPixelData;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.classic.Session;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import rafalmag.databaser.db.HibernateUtil;
import rafalmag.databaser.db.entity.Newspaper;
import rafalmag.databaser.db.entity.WordOccurrences;
import swing2swt.layout.BorderLayout;

import com.google.common.base.Joiner;
import com.google.common.collect.Sets;

public class WordCountsDialog extends Dialog {

	private final static Logger LOGGER = LoggerFactory
			.getLogger(WordCountsDialog.class);

	protected Object result;
	protected Shell shlWordsCounts;

	private final SessionFactory sessionFactory = HibernateUtil
			.getSessionFactory();
	private TreeViewer treeViewer;

	private List<WordOccurrences> rootWordOccurrencesList;

	private Button btnSave;

	/**
	 * Create the dialog.
	 * 
	 * @param parent
	 * @param style
	 */
	public WordCountsDialog(Shell parent, int style) {
		super(parent, style);
		setText("SWT Dialog");
		Transaction transaction = getSession().getTransaction();
		if (transaction.isActive()) {
			LOGGER.debug("Rolling back old transaction");
			transaction.rollback();
		}
	}

	/**
	 * Open the dialog.
	 * 
	 * @return the result
	 */
	public Object open() {
		createContents();
		shlWordsCounts.open();
		shlWordsCounts.layout();
		Display display = getParent().getDisplay();
		while (!shlWordsCounts.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		return result;
	}

	/**
	 * Create contents of the dialog.
	 */
	private void createContents() {
		shlWordsCounts = new Shell(getParent(), getStyle());
		shlWordsCounts.setSize(600, 700);
		shlWordsCounts.setText("Words counts");
		shlWordsCounts.setLayout(new BorderLayout(0, 0));

		Composite composite = new Composite(shlWordsCounts, SWT.NONE);
		composite.setLayoutData(BorderLayout.CENTER);
		initTable(composite);

		btnSave = new Button(shlWordsCounts, SWT.NONE);
		btnSave.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				LOGGER.info("Saving merge state");
				try {

					Session session = getSession();
					Transaction transaction = session.getTransaction();
					if (transaction.isActive()) {
						transaction.commit();
					} else {
						LOGGER.debug("Transaction was not active, begining new transnaction");
						session.beginTransaction();
					}
					checkState();
				} catch (HibernateException e) {
					reportError(
							"Could not save merge state because of "
									+ e.getMessage(), e);
				}
				LOGGER.info("After saving merge state");
			}
		});
		btnSave.setLayoutData(BorderLayout.SOUTH);
		btnSave.setText("Save");

		Composite composite_1 = new Composite(shlWordsCounts, SWT.NONE);
		composite_1.setLayoutData(BorderLayout.NORTH);
		composite_1.setLayout(new BorderLayout(0, 0));

		Button btnMerge = new Button(composite_1, SWT.NONE);
		btnMerge.setLayoutData(BorderLayout.CENTER);
		btnMerge.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				LOGGER.debug("merge");
				TreeSelection selection = (TreeSelection) treeViewer
						.getSelection();
				@SuppressWarnings("unchecked")
				List<WordOccurrences> list = selection.toList();
				Collections.sort(list);
				if (!selection.isEmpty()) {
					WordMergeDialog dialog = new WordMergeDialog(
							shlWordsCounts, shlWordsCounts.getStyle()
									| SWT.APPLICATION_MODAL, list);
					String mergeName = dialog.open();
					LOGGER.info("Merge parent: " + mergeName + " of: " + list);
					if (mergeName == null) {
						return; // nothing to do
					}
					try {
						Session session = getSession();
						if (session.getTransaction().isActive()) {
							LOGGER.debug("transaction is active");
						} else {
							session.beginTransaction();
							LOGGER.debug("beging new transaction");
						}

						WordOccurrences parentWO = new WordOccurrences();
						parentWO.setWord(mergeName);
						for (WordOccurrences wo : list) {
							WordOccurrences oldParent = wo.getBaseForm();
							if (oldParent != null) {
								// it had an old parent
								LOGGER.debug("deleting old parent {}",
										oldParent);
								session.delete(oldParent);
								rootWordOccurrencesList.remove(oldParent);
							}
							LOGGER.debug("updating {}", wo);
							wo.setBaseForm(parentWO);
							session.update(wo);
							rootWordOccurrencesList.remove(wo);
						}
						LOGGER.debug("saving new {}", parentWO);
						session.save(parentWO);
						rootWordOccurrencesList.add(parentWO);
						treeViewer.refresh();
						checkState();
					} catch (HibernateException e) {
						reportError("Could not merge " + list + " because of "
								+ e.getMessage(), e);
					}

				}
			}
		});
		btnMerge.setText("Merge");

		Button btnUnmerge = new Button(composite_1, SWT.NONE);
		btnUnmerge.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				LOGGER.debug("unMerge");
				TreeSelection selection = (TreeSelection) treeViewer
						.getSelection();
				@SuppressWarnings("unchecked")
				List<WordOccurrences> selectedWOs = selection.toList();
				if (!selection.isEmpty()) {
					try {
						Session session = getSession();
						if (session.getTransaction().isActive()) {
							LOGGER.debug("transaction is active");
						} else {
							session.beginTransaction();
							LOGGER.debug("beging new transaction");
						}
						for (WordOccurrences wo : selectedWOs) {
							if (wo.getBaseForm() == null) {
								// unmerging groupingNode
								SortedSet<WordOccurrences> otherForms = Sets
										.newTreeSet(wo.getOtherForms());
								if (otherForms.size() > 0) {
									LOGGER.debug("deleting {}", wo);
									session.delete(wo);
									rootWordOccurrencesList.remove(wo);
								}
								for (WordOccurrences childWO : otherForms) {
									// child will have no group
									childWO.setBaseForm(null);
									LOGGER.debug("updating {}", childWO);
									session.update(childWO);
									rootWordOccurrencesList.add(childWO);
								}
							} else {
								// childNode
								// child will have no group
								WordOccurrences parentWO = wo.getBaseForm();
								SortedSet<WordOccurrences> otherFormsOfParent = parentWO
										.getOtherForms();
								if (otherFormsOfParent.size() == 1) {
									LOGGER.debug("deleting parent" + parentWO);
									session.delete(parentWO);
									rootWordOccurrencesList.remove(parentWO);
								}
								wo.setBaseForm(null);
								LOGGER.debug("updating " + wo);
								session.update(wo);
								rootWordOccurrencesList.add(wo);
							}
						}
						checkState();
						treeViewer.refresh();
					} catch (HibernateException e) {
						reportError("Could not merge " + selectedWOs
								+ " because of " + e.getMessage(), e);
					}
				}
			}
		});
		btnUnmerge.setLayoutData(BorderLayout.EAST);
		btnUnmerge.setText("UnMerge");

		btnSave.setText("Save "
				+ WordOccurrences.countWords(rootWordOccurrencesList));
	}

	protected void checkState() {
		String after = WordOccurrences.countWords(rootWordOccurrencesList);
		String before = btnSave.getText().replace("Save ", "");
		if (before.equals(after)) {
			// ok
		} else {
			String msg = "State changed, before:\n" + before + "\n after:\n"
					+ after;
			reportError(msg);
			btnSave.setText("Save " + after);
		}

	}

	protected void reportError(String msg) {
		reportError(msg, null);
	}

	protected void reportError(String msg, @Nullable Throwable e) {
		if (e == null) {
			LOGGER.error(msg);
		} else {
			LOGGER.error(msg, e);
		}
		MessageBox mb = new MessageBox(shlWordsCounts, SWT.ICON_ERROR);
		mb.setText("Error");
		mb.setMessage(msg
				+ "\n Do not save word count. Close this windows and reopen it.");
		mb.open();
	}

	protected void initTable(Composite composite) {
		TreeColumnLayout tcl_composite = new TreeColumnLayout();
		composite.setLayout(tcl_composite);

		Tree wordsTree = new Tree(composite, SWT.BORDER | SWT.H_SCROLL
				| SWT.V_SCROLL | SWT.MULTI);
		wordsTree.setHeaderVisible(true);
		wordsTree.setLinesVisible(true);
		treeViewer = new TreeViewer(wordsTree);

		TreeColumn trclmnWord = new TreeColumn(wordsTree, SWT.NONE);
		tcl_composite.setColumnData(trclmnWord, new ColumnPixelData(200, true,
				true));
		trclmnWord.setText("Word");

		new StringTreeViewerColumnSorter(treeViewer, trclmnWord, 0);
		// trclmnWordSorter
		// .setSorter(trclmnWordSorter, TreeViewerColumnSorter.ASC);

		Session session = getSession();
		Transaction transaction = session.beginTransaction();
		@SuppressWarnings("unchecked")
		List<Newspaper> newspapers = session.createCriteria(Newspaper.class)
				.list();
		for (int i = 0; i < newspapers.size(); i++) {
			TreeColumn trclmn = new TreeColumn(wordsTree, SWT.NONE);
			tcl_composite.setColumnData(trclmn, new ColumnPixelData(100, true,
					true));
			trclmn.setText(newspapers.get(i).getTitle());

			new LongTreeViewerColumnSorter(treeViewer, trclmn, 1 + i);
		}
		@SuppressWarnings("unchecked")
		List<WordOccurrences> list = session
				.createCriteria(WordOccurrences.class)
				.add(Restrictions.isNull("baseForm")).list();
		LOGGER.info("Got WO from database: {}", list.size());
		rootWordOccurrencesList = new ArrayList<>(list);
		transaction.rollback();
		// LOGGER.debug("After sorting");

		treeViewer.setContentProvider(new WordOccurrencesContentProvider());
		treeViewer.setLabelProvider(new WordOccurrencesTableLabelProvider(
				newspapers));

		treeViewer.addSelectionChangedListener(new ISelectionChangedListener() {

			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				if (LOGGER.isDebugEnabled()) {
					if (event.getSelection().isEmpty()) {
						return;
					}
					if (event.getSelection() instanceof IStructuredSelection) {
						IStructuredSelection selection = (IStructuredSelection) event
								.getSelection();
						List<?> wos = selection.toList();
						LOGGER.debug("selected " + wos.size() + " rows:\n"
								+ Joiner.on("\n").join(wos));
					}
				}
			}
		});

		treeViewer.setInput(rootWordOccurrencesList);
	}

	protected Session getSession() {
		return sessionFactory.getCurrentSession();
	}
}
