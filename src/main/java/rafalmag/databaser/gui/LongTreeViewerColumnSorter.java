package rafalmag.databaser.gui;

import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.widgets.TreeColumn;

public class LongTreeViewerColumnSorter extends TreeViewerColumnSorter {

	final int colIdx;

	public LongTreeViewerColumnSorter(TreeViewer viewer, TreeColumn column,
			int colIdx) {
		super(viewer, column);
		this.colIdx = colIdx;
	}

	@Override
	protected int doCompare(Viewer v, Object e1, Object e2) {
		ITableLabelProvider lp = ((ITableLabelProvider) viewer
				.getLabelProvider());
		String t1 = lp.getColumnText(e1, colIdx);
		Long l1 = Long.parseLong(t1);
		String t2 = lp.getColumnText(e2, colIdx);
		Long l2 = Long.parseLong(t2);
		return l1.compareTo(l2);
	}

}
