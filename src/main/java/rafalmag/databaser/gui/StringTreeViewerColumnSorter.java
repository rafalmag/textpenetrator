package rafalmag.databaser.gui;

import java.util.Comparator;

import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.widgets.TreeColumn;

import rafalmag.databaser.db.PlStringComparator;

public class StringTreeViewerColumnSorter extends TreeViewerColumnSorter {

	final int colIdx;
	private final Comparator<Object> plCollator;

	public StringTreeViewerColumnSorter(TreeViewer viewer, TreeColumn column,
			int colIdx) {
		super(viewer, column);
		this.colIdx = colIdx;
		PlStringComparator comparator = new PlStringComparator();
		plCollator = comparator.getComparator();
	}

	@Override
	protected int doCompare(Viewer v, Object e1, Object e2) {
		ITableLabelProvider lp = ((ITableLabelProvider) viewer
				.getLabelProvider());
		String t1 = lp.getColumnText(e1, colIdx);
		String t2 = lp.getColumnText(e2, colIdx);
		return plCollator.compare(t1, t2);
	}

}
