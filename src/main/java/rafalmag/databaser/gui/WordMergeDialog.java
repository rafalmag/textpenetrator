package rafalmag.databaser.gui;

import java.util.List;

import javax.annotation.Nullable;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

import rafalmag.databaser.db.entity.WordOccurrences;
import swing2swt.layout.BorderLayout;

public class WordMergeDialog extends Dialog {

	protected String result;
	protected Shell shell;
	private final List<WordOccurrences> list;

	/**
	 * Create the dialog.
	 * 
	 * @param parent
	 * @param style
	 * @param list
	 */
	public WordMergeDialog(Shell parent, int style, List<WordOccurrences> list) {
		super(parent, style);
		this.list = list;
		setText("Words merge dialog");
	}

	/**
	 * Open the dialog.
	 * 
	 * @return the combo text or null if cancelled
	 */
	@Nullable
	public String open() {
		createContents();
		shell.open();
		shell.layout();
		Display display = getParent().getDisplay();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		return result;
	}

	/**
	 * Create contents of the dialog.
	 */
	private void createContents() {
		shell = new Shell(getParent(), getStyle() | SWT.DIALOG_TRIM
				| SWT.RESIZE | SWT.APPLICATION_MODAL);
		shell.setSize(225, 113);
		shell.setText(getText());
		shell.setLayout(new BorderLayout(0, 0));

		Label lblChooseNameFor = new Label(shell, SWT.NONE);
		lblChooseNameFor.setLayoutData(BorderLayout.NORTH);
		lblChooseNameFor.setText("Choose name for merged words");

		final Combo combo = new Combo(shell, SWT.NONE);
		combo.setLayoutData(BorderLayout.CENTER);
		for (WordOccurrences wo : list) {
			combo.add(wo.getWord());
		}
		combo.select(0);

		Button btnOk = new Button(shell, SWT.NONE);
		btnOk.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				result = combo.getText();
				shell.close();
			}
		});
		btnOk.setLayoutData(BorderLayout.SOUTH);
		btnOk.setText("Ok");
	}
}
