package rafalmag.databaser.gui;

import java.util.List;

import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.swt.graphics.Image;

import rafalmag.databaser.db.entity.Newspaper;
import rafalmag.databaser.db.entity.WordOccurrences;

public class WordOccurrencesTableLabelProvider implements ITableLabelProvider {

	private final List<Newspaper> newspapers;

	public WordOccurrencesTableLabelProvider(List<Newspaper> newspapers) {
		this.newspapers = newspapers;
	}

	@Override
	public void addListener(ILabelProviderListener arg0) {
	}

	@Override
	public void dispose() {
	}

	@Override
	public boolean isLabelProperty(Object arg0, String arg1) {
		return false;
	}

	@Override
	public void removeListener(ILabelProviderListener arg0) {
	}

	@Override
	public Image getColumnImage(Object arg0, int arg1) {
		return null;
	}

	@Override
	public String getColumnText(Object element, int columnIndex) {
		if (element instanceof WordOccurrences) {
			WordOccurrences wo = (WordOccurrences) element;
			if (columnIndex == 0) {
				return wo.getWord();
			} else {
				Newspaper newspaper = newspapers.get(columnIndex - 1);
				Long occurrences = wo.getOccurrencesFor(newspaper);
				if (occurrences == null) {
					return "0";
				} else {
					return occurrences.toString();
				}
			}
		} else {
			if (columnIndex == 0) {
				return element.toString();
			}
		}
		return null;
	}

}
