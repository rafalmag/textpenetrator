package rafalmag.databaser.gui;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.annotation.Nullable;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import rafalmag.databaser.db.DbPurger;
import rafalmag.databaser.db.HibernateUtil;
import rafalmag.databaser.db.RecountWords;
import rafalmag.databaser.db.analyzer.ToCsv;
import rafalmag.databaser.db.analyzer.Words;
import rafalmag.databaser.db.loader.DirectoryLoader;
import rafalmag.databaser.morf.MorfUtils;

public class Databaser3Window {

	private final static Logger LOGGER = LoggerFactory
			.getLogger(Databaser3Window.class);

	private final SessionFactory sessionFactory = HibernateUtil
			.getSessionFactory();

	protected Shell shlDatabaser;

	/**
	 * Launch the application.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		checkEnv();
		try {
			Databaser3Window window = new Databaser3Window();
			window.open();
		} catch (Exception e) {
			LOGGER.error(
					"Could not open " + Databaser3Window.class.getSimpleName()
							+ " frame becaue of " + e.getMessage(), e);
		}
	}

	private static void checkEnv() {
		String arch = System.getProperty("os.arch");
		if (arch.equals("x86")) {
			// ok
			return;
		} else {
			LOGGER.warn("Only x86 platform is supported, not: " + arch);
		}

	}

	/**
	 * Open the window.
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents();
		shlDatabaser.open();
		shlDatabaser.layout();
		while (!shlDatabaser.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shlDatabaser = new Shell();
		shlDatabaser.setSize(450, 300);
		shlDatabaser.setText("Databaser3");
		shlDatabaser.setLayout(new FillLayout(SWT.HORIZONTAL));

		Composite composite = new Composite(shlDatabaser, SWT.NONE);
		composite.setLayout(new RowLayout(SWT.HORIZONTAL));

		Button btnPurgeDb = new Button(composite, SWT.NONE);
		btnPurgeDb.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				if (doReallyPurge()) {
					try {
						Session session = sessionFactory.getCurrentSession();
						Transaction transaction = session.beginTransaction();
						new DbPurger(session).purge();
						transaction.commit();
					} catch (HibernateException e) {
						LOGGER.error(
								"Could not purge db because of "
										+ e.getMessage(), e);
					}
				}
			}

			private boolean doReallyPurge() {
				MessageBox messageBox = new MessageBox(shlDatabaser,
						SWT.ICON_WARNING | SWT.YES | SWT.NO);
				messageBox
						.setMessage("Do you really want to purge (clean/wipe) database?");
				messageBox.setText("Purge databse");
				int response = messageBox.open();
				return response == SWT.YES;
			}
		});
		btnPurgeDb.setText("Purge Db");

		Button btnLoadDir = new Button(composite, SWT.NONE);
		btnLoadDir.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				File selectedFile = getDirectory();
				if (selectedFile == null) {
					return;
				}
				try {
					Session session = sessionFactory.getCurrentSession();
					Transaction transaction = session.beginTransaction();
					LOGGER.info("Loading directory {}...", selectedFile);
					new DirectoryLoader(session).loadDirectory(selectedFile);
					LOGGER.info("After loading directory {}. Before commit...",
							selectedFile);
					transaction.commit();
					LOGGER.info(
							"After loading directory {} transaction commit",
							selectedFile);
				} catch (HibernateException | IOException e1) {
					LOGGER.error("Could not load directory: " + selectedFile
							+ " because of " + e1.getMessage(), e1);
				}
			}
		});
		btnLoadDir.setText("Load dir");

		Button btnSaveWordList = new Button(composite, SWT.NONE);
		btnSaveWordList.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				List<List<String>> rows;
				try {
					Session session = sessionFactory.getCurrentSession();
					Transaction transaction = session.beginTransaction();
					rows = Words.getWords(session);
					transaction.commit();
				} catch (HibernateException e) {
					LOGGER.error(
							"Could not get words because of " + e.getMessage(),
							e);
					return;
				}
				File selectedFile = getFile("words.csv");
				if (selectedFile == null) {
					return;
				}
				try {
					ToCsv.save(selectedFile, rows);
				} catch (IOException e1) {
					LOGGER.error(
							"Could not save words because of "
									+ e1.getMessage(), e1);
					return;
				}

			}
		});
		btnSaveWordList.setText("Save word list");

		Button btnRecountWords = new Button(composite, SWT.NONE);
		btnRecountWords.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent event) {
				MorfUtils morfUtils = getMorfUtils();
				if (morfUtils != null) {
					Session session = sessionFactory.getCurrentSession();
					Transaction transaction = session.beginTransaction();
					new RecountWords(session, morfUtils).recountWords();
					LOGGER.info("Before transaction commit...");
					Double startTimeSec = System.currentTimeMillis() / 1000.0;
					transaction.commit();
					Double seconds = System.currentTimeMillis() / 1000.0
							- startTimeSec;
					LOGGER.info("After transaction commit. Took {}s", seconds);
				}
			}

			@Nullable
			private MorfUtils getMorfUtils() {
				MorfUtilsDialog dialog = new MorfUtilsDialog(shlDatabaser,
						shlDatabaser.getStyle() | SWT.APPLICATION_MODAL);
				return dialog.open();
			}
		});
		btnRecountWords.setText("Recount words");

		Button btnWordCount = new Button(composite, SWT.NONE);
		btnWordCount.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				LOGGER.debug("Word count button pressed event={}", event);
				WordCountsDialog dialog = new WordCountsDialog(shlDatabaser,
						shlDatabaser.getStyle() | SWT.APPLICATION_MODAL);
				dialog.open();
			}
		});
		btnWordCount.setText("Word count");

		Button btnSaveWordOccurrences = new Button(composite, SWT.NONE);
		btnSaveWordOccurrences.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				LOGGER.debug("Save word occurrences button pressed event={}",
						event);
				List<List<String>> wordCountsAsLists;
				try {
					Session session = sessionFactory.getCurrentSession();
					Transaction transaction = session.beginTransaction();
					wordCountsAsLists = Words.getWordCountsAsLists(session);
					transaction.rollback();
				} catch (HibernateException e) {
					LOGGER.error(
							"Could not get word count because of "
									+ e.getMessage(), e);
					return;
				}
				File selectedFile = getFile("wordCount.csv");
				if (selectedFile == null) {
					return;
				}
				try {
					ToCsv.save(selectedFile, wordCountsAsLists);
				} catch (IOException e) {
					LOGGER.error(
							"Could not save word count because of "
									+ e.getMessage(), e);
					return;
				}
			}
		});
		btnSaveWordOccurrences.setText("Save word occurrences");

		shlDatabaser.pack();

	}

	String selectedFile = ".";

	protected File getFile(String string) {
		FileDialog dialog = new FileDialog(shlDatabaser, SWT.SAVE);
		dialog.setFilterNames(new String[] { "CSV", "All Files (*.*)" });
		dialog.setFilterExtensions(new String[] { "*.csv", "*.*" });
		dialog.setFileName(string);
		String file = dialog.open();
		if (file == null) {
			return null;
		} else {
			selectedFile = file;
			return new File(file);
		}
	}

	String selectedDir = new File(".").getAbsolutePath();

	protected File getDirectory() {
		DirectoryDialog directoryDialog = new DirectoryDialog(shlDatabaser);

		directoryDialog.setFilterPath(selectedDir);
		directoryDialog.setMessage("Select .doc directory");

		String dir = directoryDialog.open();
		if (dir == null) {
			return null;
		} else {
			selectedDir = dir;
			return new File(dir);
		}

	}

}
