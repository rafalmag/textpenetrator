package rafalmag.databaser.gui;

import java.util.Collection;

import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;

import rafalmag.databaser.db.entity.WordOccurrences;

public class WordOccurrencesContentProvider implements ITreeContentProvider {

	@SuppressWarnings("rawtypes")
	private final Class<Collection> rootClass = Collection.class;

	@Override
	public Object[] getChildren(Object parentElement) {

		if (rootClass.isAssignableFrom(parentElement.getClass())) {
			return rootClass.cast(parentElement).toArray();
		}
		if (parentElement instanceof WordOccurrences) {
			return ((WordOccurrences) parentElement).getOtherForms().toArray();
		}
		return new Object[0];
	}

	@Override
	public Object getParent(Object element) {
		if (element instanceof WordOccurrences) {
			return ((WordOccurrences) element).getBaseForm();
		}
		return null;
	}

	@Override
	public boolean hasChildren(Object element) {
		if (rootClass.isAssignableFrom(element.getClass())) {
			return rootClass.cast(element).size() > 0;
		}
		if (element instanceof WordOccurrences) {
			return ((WordOccurrences) element).getOtherForms().size() > 0;
		}
		return false;
	}

	@Override
	public Object[] getElements(Object listOfWO) {
		return getChildren(listOfWO);
	}

	@Override
	public void dispose() {
	}

	@Override
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
	}

}
