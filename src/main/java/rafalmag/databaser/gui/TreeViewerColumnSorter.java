package rafalmag.databaser.gui;

import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerComparator;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.TreeColumn;

// my modification of
// http://eclipsisms.blogspot.com/2007/07/tableviewer-sorting-using-labelprovider.html
public abstract class TreeViewerColumnSorter extends ViewerComparator {
	public static final int ASC = 1;

	public static final int NONE = 0;

	public static final int DESC = -1;

	private int direction = 0;

	private final TreeColumn column;

	protected final TreeViewer viewer;

	public TreeViewerColumnSorter(TreeViewer viewer, TreeColumn column) {
		this.column = column;
		this.viewer = viewer;
		this.column.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				if (TreeViewerColumnSorter.this.viewer.getComparator() != null) {
					if (TreeViewerColumnSorter.this.viewer.getComparator() == TreeViewerColumnSorter.this) {
						int tdirection = TreeViewerColumnSorter.this.direction;

						if (tdirection == ASC) {
							setSorter(TreeViewerColumnSorter.this, DESC);
						} else if (tdirection == DESC) {
							setSorter(TreeViewerColumnSorter.this, NONE);
						}
					} else {
						setSorter(TreeViewerColumnSorter.this, ASC);
					}
				} else {
					setSorter(TreeViewerColumnSorter.this, ASC);
				}
			}
		});
	}

	public void setSorter(TreeViewerColumnSorter sorter, int direction) {
		if (direction == NONE) {
			column.getParent().setSortColumn(null);
			column.getParent().setSortDirection(SWT.NONE);
			viewer.setComparator(null);
		} else {
			column.getParent().setSortColumn(column);
			sorter.direction = direction;

			if (direction == ASC) {
				column.getParent().setSortDirection(SWT.DOWN);
			} else {
				column.getParent().setSortDirection(SWT.UP);
			}

			if (viewer.getComparator() == sorter) {
				viewer.refresh();
			} else {
				viewer.setComparator(sorter);
			}

		}
	}

	@Override
	public int compare(Viewer viewer, Object e1, Object e2) {
		return direction * doCompare(viewer, e1, e2);
	}

	protected abstract int doCompare(Viewer TableViewer, Object e1, Object e2);
}